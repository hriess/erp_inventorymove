package de.frick.erp;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}
	
	public void onClick (View view) {
	
		IntentIntegrator integrator = new IntentIntegrator(this);
		integrator.initiateScan();
	
	}
	
	// }

	
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {  
		IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);  
		if (scanResult != null) {  
			
			String barcode;
			String typ;
			
			barcode = scanResult.getContents();
			typ = scanResult.getFormatName();
			
			EditText etBarcode = (EditText) findViewById(R.id.editText1);
			EditText etTyp = (EditText) findViewById(R.id.editText2);
			
			etBarcode.setText(barcode);
			etTyp.setText(typ);
			
			// handle scan result  
			} 
		
			// else continue with any other code you need in the method  ...
		// }
			
		}
	//}
}
